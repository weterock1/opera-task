package com.example.operagithubclient;

import com.example.operagithubclient.client.model.GithubClientUserGetResponseBody;
import com.example.operagithubclient.client.model.GithubClientUserRepositoriesGetResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.comparator.JSONComparator;

import java.util.List;
import java.util.function.Supplier;

public class OperaModelSerializationTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void checkUserModel() throws JsonProcessingException, JSONException {
        Supplier<String> test = () -> {
            return
                    "{\n" +
                            "  \"login\": \"weterock\",\n" +
                            "  \"id\": 22843070,\n" +
                            "  \"node_id\": \"MDQ6VXNlcjIyODQzMDcw\",\n" +
                            "  \"avatar_url\": \"https://avatars.githubusercontent.com/u/22843070?v=4\",\n" +
                            "  \"gravatar_id\": \"\",\n" +
                            "  \"url\": \"https://api.github.com/users/weterock\",\n" +
                            "  \"html_url\": \"https://github.com/weterock\",\n" +
                            "  \"followers_url\": \"https://api.github.com/users/weterock/followers\",\n" +
                            "  \"following_url\": \"https://api.github.com/users/weterock/following{/other_user}\",\n" +
                            "  \"gists_url\": \"https://api.github.com/users/weterock/gists{/gist_id}\",\n" +
                            "  \"starred_url\": \"https://api.github.com/users/weterock/starred{/owner}{/repo}\",\n" +
                            "  \"subscriptions_url\": \"https://api.github.com/users/weterock/subscriptions\",\n" +
                            "  \"organizations_url\": \"https://api.github.com/users/weterock/orgs\",\n" +
                            "  \"repos_url\": \"https://api.github.com/users/weterock/repos\",\n" +
                            "  \"events_url\": \"https://api.github.com/users/weterock/events{/privacy}\",\n" +
                            "  \"received_events_url\": \"https://api.github.com/users/weterock/received_events\",\n" +
                            "  \"type\": \"User\",\n" +
                            "  \"site_admin\": false,\n" +
                            "  \"name\": null,\n" +
                            "  \"company\": null,\n" +
                            "  \"blog\": \"\",\n" +
                            "  \"location\": null,\n" +
                            "  \"email\": null,\n" +
                            "  \"hireable\": null,\n" +
                            "  \"bio\": null,\n" +
                            "  \"twitter_username\": null,\n" +
                            "  \"public_repos\": 1,\n" +
                            "  \"public_gists\": 0,\n" +
                            "  \"followers\": 0,\n" +
                            "  \"following\": 0,\n" +
                            "  \"created_at\": \"2016-10-14T17:22:44Z\",\n" +
                            "  \"updated_at\": \"2022-04-07T14:48:13Z\"\n" +
                            "}";
        };


        String result = objectMapper.writeValueAsString(objectMapper.readValue(test.get(), GithubClientUserGetResponseBody.class));

        JSONAssert.assertEquals(test.get(), result, false);
    }

    @Test
    public void checkUserRepositoriesModel() throws JsonProcessingException, JSONException {
        Supplier<String> test = () -> {
            return
                   "[\n" +
                           "  {\n" +
                           "    \"id\": 472391293,\n" +
                           "    \"node_id\": \"R_kgDOHCgefQ\",\n" +
                           "    \"name\": \"atom\",\n" +
                           "    \"full_name\": \"weterock/atom\",\n" +
                           "    \"private\": false,\n" +
                           "    \"html_url\": \"https://github.com/weterock/atom\",\n" +
                           "    \"description\": null,\n" +
                           "    \"fork\": true,\n" +
                           "    \"url\": \"https://api.github.com/repos/weterock/atom\",\n" +
                           "    \"forks_url\": \"https://api.github.com/repos/weterock/atom/forks\",\n" +
                           "    \"keys_url\": \"https://api.github.com/repos/weterock/atom/keys{/key_id}\",\n" +
                           "    \"collaborators_url\": \"https://api.github.com/repos/weterock/atom/collaborators{/collaborator}\",\n" +
                           "    \"teams_url\": \"https://api.github.com/repos/weterock/atom/teams\",\n" +
                           "    \"hooks_url\": \"https://api.github.com/repos/weterock/atom/hooks\",\n" +
                           "    \"issue_events_url\": \"https://api.github.com/repos/weterock/atom/issues/events{/number}\",\n" +
                           "    \"events_url\": \"https://api.github.com/repos/weterock/atom/events\",\n" +
                           "    \"assignees_url\": \"https://api.github.com/repos/weterock/atom/assignees{/user}\",\n" +
                           "    \"branches_url\": \"https://api.github.com/repos/weterock/atom/branches{/branch}\",\n" +
                           "    \"tags_url\": \"https://api.github.com/repos/weterock/atom/tags\",\n" +
                           "    \"blobs_url\": \"https://api.github.com/repos/weterock/atom/git/blobs{/sha}\",\n" +
                           "    \"git_tags_url\": \"https://api.github.com/repos/weterock/atom/git/tags{/sha}\",\n" +
                           "    \"git_refs_url\": \"https://api.github.com/repos/weterock/atom/git/refs{/sha}\",\n" +
                           "    \"trees_url\": \"https://api.github.com/repos/weterock/atom/git/trees{/sha}\",\n" +
                           "    \"statuses_url\": \"https://api.github.com/repos/weterock/atom/statuses/{sha}\",\n" +
                           "    \"languages_url\": \"https://api.github.com/repos/weterock/atom/languages\",\n" +
                           "    \"stargazers_url\": \"https://api.github.com/repos/weterock/atom/stargazers\",\n" +
                           "    \"contributors_url\": \"https://api.github.com/repos/weterock/atom/contributors\",\n" +
                           "    \"subscribers_url\": \"https://api.github.com/repos/weterock/atom/subscribers\",\n" +
                           "    \"subscription_url\": \"https://api.github.com/repos/weterock/atom/subscription\",\n" +
                           "    \"commits_url\": \"https://api.github.com/repos/weterock/atom/commits{/sha}\",\n" +
                           "    \"git_commits_url\": \"https://api.github.com/repos/weterock/atom/git/commits{/sha}\",\n" +
                           "    \"comments_url\": \"https://api.github.com/repos/weterock/atom/comments{/number}\",\n" +
                           "    \"issue_comment_url\": \"https://api.github.com/repos/weterock/atom/issues/comments{/number}\",\n" +
                           "    \"contents_url\": \"https://api.github.com/repos/weterock/atom/contents/{+path}\",\n" +
                           "    \"compare_url\": \"https://api.github.com/repos/weterock/atom/compare/{base}...{head}\",\n" +
                           "    \"merges_url\": \"https://api.github.com/repos/weterock/atom/merges\",\n" +
                           "    \"archive_url\": \"https://api.github.com/repos/weterock/atom/{archive_format}{/ref}\",\n" +
                           "    \"downloads_url\": \"https://api.github.com/repos/weterock/atom/downloads\",\n" +
                           "    \"issues_url\": \"https://api.github.com/repos/weterock/atom/issues{/number}\",\n" +
                           "    \"pulls_url\": \"https://api.github.com/repos/weterock/atom/pulls{/number}\",\n" +
                           "    \"milestones_url\": \"https://api.github.com/repos/weterock/atom/milestones{/number}\",\n" +
                           "    \"notifications_url\": \"https://api.github.com/repos/weterock/atom/notifications{?since,all,participating}\",\n" +
                           "    \"labels_url\": \"https://api.github.com/repos/weterock/atom/labels{/name}\",\n" +
                           "    \"releases_url\": \"https://api.github.com/repos/weterock/atom/releases{/id}\",\n" +
                           "    \"deployments_url\": \"https://api.github.com/repos/weterock/atom/deployments\",\n" +
                           "    \"created_at\": \"2022-03-21T15:11:10Z\",\n" +
                           "    \"updated_at\": \"2016-10-12T14:46:25Z\",\n" +
                           "    \"pushed_at\": \"2016-12-20T17:25:17Z\",\n" +
                           "    \"git_url\": \"git://github.com/weterock/atom.git\",\n" +
                           "    \"ssh_url\": \"git@github.com:weterock/atom.git\",\n" +
                           "    \"clone_url\": \"https://github.com/weterock/atom.git\",\n" +
                           "    \"svn_url\": \"https://github.com/weterock/atom\",\n" +
                           "    \"homepage\": null,\n" +
                           "    \"size\": 33113,\n" +
                           "    \"stargazers_count\": 0,\n" +
                           "    \"watchers_count\": 0,\n" +
                           "    \"language\": null\n" +
                           "  }\n" +
                           "]";
        };


        String result = objectMapper.writeValueAsString(objectMapper.readValue(test.get(), new TypeReference<List<GithubClientUserRepositoriesGetResponseBody>>() {
        }));

        JSONAssert.assertEquals(test.get(), result, false);
    }
}
