package com.example.operagithubclient.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient;

@Component
public class GithubClient {

    private WebClient webClient;

    @Value("${opera.github.url}")
    private String host;
    public static final String GITHUB_CONTEXT_CLIENT = "github-client";

    public GithubClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public <T> Mono<T> sendGetGithubRequest(
            String url,
            ParameterizedTypeReference<T> mapToType
    ) {
        return Mono.deferContextual(Mono::just)
                .flatMap(ctx -> {
                    OAuth2AuthorizedClient client = (OAuth2AuthorizedClient) ctx.get(GITHUB_CONTEXT_CLIENT);
                    return webClient.get()
                            .uri(host + url)
                            .attributes(oauth2AuthorizedClient(client))
                            .retrieve()
                            .bodyToMono(mapToType);
                });
    }
}
