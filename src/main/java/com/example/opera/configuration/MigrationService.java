package com.example.opera.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.r2dbc.core.DatabaseClient;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
public class MigrationService {

    private DatabaseClient databaseClient;

    public MigrationService(DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }

    @PostConstruct
    public void initSchema() {
        // TODO
        // Could be replaces with liquibase
            Arrays.asList("DROP TABLE IF EXISTS github_user_entity;",
                    "DROP TABLE IF EXISTS github_user_repository_entity;",
                    "CREATE TABLE github_user_entity (login varchar primary key, html_url varchar, note varchar, create_time bigint);",
                    "CREATE TABLE github_user_repository_entity (id varchar primary key, login varchar, node_id varchar, name varchar, is_private varchar, full_name varchar, html_url varchar, description varchar, create_time bigint);")
                    .forEach(s ->
                            databaseClient.sql(s)
                                    .fetch()
                                    .rowsUpdated()
                                    .block()
                    );

    }

}
