package com.example.opera;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import reactor.tools.agent.ReactorDebugAgent;

import java.util.Arrays;

@SpringBootApplication
@EnableR2dbcRepositories
@EntityScan(basePackages = {"com.example.opera"})
@OpenAPIDefinition(info = @Info(title = "Opera test task", version = "0.0.1", description = "Application for gaining info about user repositories"))
public class OperaApplication {

    public static void main(String[] args) {
        ReactorDebugAgent.init();
        SpringApplication.run(OperaApplication.class, args);
    }

}
