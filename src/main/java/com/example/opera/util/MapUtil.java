package com.example.opera.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// For sure you may use better solution for mapping to eliminate double serialization
@Component
public class MapUtil {

    @Autowired
    ObjectMapper objectMapper;

    // Could be used for model with same serialization type
    public <Response, Request> Response mapToClass(Request anyObject, TypeReference<Response> responseTypeReference) {
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(anyObject),
                    responseTypeReference);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
