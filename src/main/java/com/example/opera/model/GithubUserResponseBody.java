package com.example.opera.model;

public class GithubUserResponseBody {

    private String login;
    private String htmlUrl;
    private String note;

    private Boolean isActual;

    public GithubUserResponseBody() {
    }

    public GithubUserResponseBody(String login, String htmlUrl, String note) {
        this.login = login;
        this.htmlUrl = htmlUrl;
        this.note = note;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(Boolean isActual) {
        this.isActual = isActual;
    }
}
