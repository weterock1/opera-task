package com.example.opera.model;

import java.util.List;

public class GithubUserRepositoriesResponseBody {

    private List<GithubUserRepositoryModel> repositories;
    private Boolean isActual;

    public GithubUserRepositoriesResponseBody() {
    }

    public GithubUserRepositoriesResponseBody(List<GithubUserRepositoryModel> repositories, Boolean isActual) {
        this.repositories = repositories;
        this.isActual = isActual;
    }

    public List<GithubUserRepositoryModel> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<GithubUserRepositoryModel> repositories) {
        this.repositories = repositories;
    }

    public Boolean getActual() {
        return isActual;
    }

    public void setActual(Boolean actual) {
        isActual = actual;
    }
}
