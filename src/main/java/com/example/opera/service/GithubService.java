package com.example.opera.service;

import com.example.opera.datasource.GithubUserEntity;
import com.example.opera.datasource.GithubUserRepositoryEntity;
import com.example.opera.model.GithubUserRepositoriesResponseBody;
import com.example.opera.model.GithubUserRepositoryModel;
import com.example.opera.model.GithubUserResponseBody;
import com.example.opera.util.MapUtil;
import com.example.opera.util.Tuple2;
import com.example.operagithubclient.client.GithubClient;
import com.example.operagithubclient.client.model.GithubClientUserGetResponseBody;
import com.example.operagithubclient.client.model.GithubClientUserRepositoriesGetResponseBody;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GithubService {

    private Logger log = LoggerFactory.getLogger(GithubService.class);

    private final GithubClient githubClient;
    private final GithubDatasourceService githubDatasourceService;
    private final MapUtil mapUtil;


    public GithubService(GithubClient githubClient,
                         GithubDatasourceService githubDatasourceService,
                         MapUtil mapUtil) {
        this.githubClient = githubClient;
        this.githubDatasourceService = githubDatasourceService;
        this.mapUtil = mapUtil;
    }

    public Mono<GithubUserRepositoriesResponseBody> getUserReposInfo(String userName, Boolean fetchInfo) {
        return Mono.deferContextual(Mono::just)
                .flatMap(ctx -> {
                    if (fetchInfo != null) {
                        if (fetchInfo) {
                            return fetchInfoAboutRepositories(userName)
                                    .map(repo ->
                                            new GithubUserRepositoriesResponseBody(
                                                    repo,
                                                    Boolean.TRUE
                                            )
                                    )
                                    .doOnNext(repo -> {
                                        githubDatasourceService.saveUserRepositoriesEntity(
                                                ctx,
                                                userName,
                                                repo.getRepositories()
                                                        .stream()
                                                        .map(repoB -> GithubUserRepositoryEntity.of(
                                                                repoB.getId(),
                                                                userName,
                                                                repoB.getNodeId(),
                                                                repoB.getName(),
                                                                repoB.getIsPrivate(),
                                                                repoB.getFullName(),
                                                                repoB.getHtmlUrl(),
                                                                repoB.getDescription()
                                                        ))
                                                        .collect(Collectors.toList())
                                        );
                                    });
                        } else {
                            return githubDatasourceService.getUserRepositories(userName)
                                    .map(listOfRepositories ->
                                            new GithubUserRepositoriesResponseBody(
                                                    listOfRepositories
                                                            .stream()
                                                            .map(repoEntity -> mapUtil.mapToClass(repoEntity.getT1(), new TypeReference<GithubUserRepositoryModel>() {
                                                            }))
                                                            .collect(Collectors.toList()),
                                                    listOfRepositories.isEmpty() ? null : listOfRepositories.get(0).getT2()
                                            )
                                    );
                        }
                    } else {
                        return githubDatasourceService.getUserRepositories(userName)
                                .flatMap(listOfRepositories -> {
                                            if (!listOfRepositories.isEmpty() && listOfRepositories.get(0).getT2()) {
                                                return Mono.just(
                                                        new GithubUserRepositoriesResponseBody(
                                                                listOfRepositories
                                                                        .stream()
                                                                        .map(repoEntity -> mapUtil.mapToClass(repoEntity.getT1(), new TypeReference<GithubUserRepositoryModel>() {
                                                                        }))
                                                                        .collect(Collectors.toList()),
                                                                listOfRepositories.isEmpty() ? null : listOfRepositories.get(0).getT2()
                                                        )
                                                );
                                            } else {
                                                return fetchInfoAboutRepositories(userName)
                                                        .map(repo ->
                                                                new GithubUserRepositoriesResponseBody(
                                                                        repo,
                                                                        Boolean.TRUE
                                                                )
                                                        )
                                                        .doOnNext(repo -> {
                                                            githubDatasourceService.saveUserRepositoriesEntity(
                                                                    ctx,
                                                                    userName,
                                                                    repo.getRepositories()
                                                                            .stream()
                                                                            .map(repoB -> GithubUserRepositoryEntity.of(
                                                                                    repoB.getId(),
                                                                                    userName,
                                                                                    repoB.getNodeId(),
                                                                                    repoB.getName(),
                                                                                    repoB.getIsPrivate(),
                                                                                    repoB.getFullName(),
                                                                                    repoB.getHtmlUrl(),
                                                                                    repoB.getDescription()
                                                                            ))
                                                                            .collect(Collectors.toList())
                                                            );
                                                        })
                                                        .onErrorResume(Exception.class, ex -> {
                                                            log.error("Error: {}", ex.getLocalizedMessage());
                                                            return Mono.just(
                                                                    new GithubUserRepositoriesResponseBody(
                                                                            listOfRepositories
                                                                                    .stream()
                                                                                    .map(repoEntity -> mapUtil.mapToClass(repoEntity.getT1(), new TypeReference<GithubUserRepositoryModel>() {
                                                                                    }))
                                                                                    .collect(Collectors.toList()),
                                                                            listOfRepositories.isEmpty() ? null : listOfRepositories.get(0).getT2()
                                                                    )
                                                            );
                                                        });
                                            }
                                        }
                                );
                    }
                });
    }

    public Mono<GithubUserResponseBody> getUserInfo(String userName, Boolean fetchInfo) {
        return Mono.deferContextual(Mono::just)
                .flatMap(ctx -> {
                    if (fetchInfo != null) {
                        if (fetchInfo) {
                            return fetchInfoAboutUser(userName)
                                    .doOnNext(user -> user.setIsActual(Boolean.TRUE))
                                    .doOnNext(
                                            user -> {
                                                githubDatasourceService.saveUserEntity(ctx, GithubUserEntity.of(
                                                        user.getLogin(),
                                                        user.getHtmlUrl(),
                                                        user.getNote()
                                                ));
                                            }
                                    );
                        } else {
                            return githubDatasourceService.getUser(userName)
                                    .map(userEntityOptional -> {
                                        if (userEntityOptional.isPresent()) {
                                            Tuple2<GithubUserEntity, Boolean> userEntity = userEntityOptional.get();
                                            GithubUserResponseBody githubUserResponseBody = mapUtil.mapToClass(userEntity.getT1(), new TypeReference<GithubUserResponseBody>() {
                                            });
                                            githubUserResponseBody.setIsActual(userEntity.getT2());
                                            return githubUserResponseBody;
                                        }
                                        throw new RuntimeException("Don`t have cached user: " + userName);
                                    });
                        }
                    } else {
                        return githubDatasourceService.getUser(userName)
                                .flatMap(userEntityOptional -> {
                                    if (userEntityOptional.isPresent()) {
                                        Tuple2<GithubUserEntity, Boolean> userEntity = userEntityOptional.get();
                                        if (!userEntity.getT2()) {
                                            return fetchInfoAboutUser(userName)
                                                    .doOnNext(user -> user.setIsActual(Boolean.TRUE))
                                                    .doOnNext(
                                                            user -> {
                                                                githubDatasourceService.saveUserEntity(ctx,
                                                                            GithubUserEntity.of(
                                                                                    user.getLogin(),
                                                                                    user.getHtmlUrl(),
                                                                                    user.getNote()
                                                                            )
                                                                        );
                                                            }
                                                    )
                                                    .onErrorResume(Exception.class, ex -> {
                                                        log.error("Error: {}", ex.getLocalizedMessage());
                                                        GithubUserResponseBody githubUserResponseBody = mapUtil.mapToClass(userEntity.getT1(), new TypeReference<GithubUserResponseBody>() {
                                                        });
                                                        githubUserResponseBody.setIsActual(userEntity.getT2());
                                                        return Mono.just(githubUserResponseBody);
                                                    });
                                        }
                                        GithubUserResponseBody githubUserResponseBody = mapUtil.mapToClass(userEntity.getT1(), new TypeReference<GithubUserResponseBody>() {
                                        });
                                        githubUserResponseBody.setIsActual(userEntity.getT2());
                                        return Mono.just(githubUserResponseBody);
                                    }
                                    return fetchInfoAboutUser(userName)
                                            .doOnNext(user -> user.setIsActual(Boolean.TRUE))
                                            .doOnNext(
                                                    user -> {
                                                        githubDatasourceService.saveUserEntity(ctx, GithubUserEntity.of(
                                                                user.getLogin(),
                                                                user.getHtmlUrl(),
                                                                user.getNote()
                                                        ));
                                                    }
                                            );
                                });
                    }
                });
    }

    public Mono<GithubUserResponseBody> fetchInfoAboutUser(String userName) {
        return githubClient.sendGetGithubRequest(new StringBuilder()
                        .append("/")
                        .append("users/")
                        .append(userName)
                        .toString(),
                new ParameterizedTypeReference<GithubClientUserGetResponseBody>() {
                }
        )
                .map(githubClientUserGetResponseBody ->
                        new GithubUserResponseBody(
                                githubClientUserGetResponseBody.getLogin(),
                                githubClientUserGetResponseBody.getHtmlUrl(),
                                githubClientUserGetResponseBody.getName()
                        )
                );
    }

    public Mono<List<GithubUserRepositoryModel>> fetchInfoAboutRepositories(String userName) {
        return githubClient.sendGetGithubRequest(new StringBuilder()
                        .append("/")
                        .append("users/")
                        .append(userName)
                        .append("/")
                        .append("repos")
                        .toString(),
                new ParameterizedTypeReference<List<GithubClientUserRepositoriesGetResponseBody>>() {
                }
        )
                .map(githubClientUserRepositoriesGetResponseBody -> {
                            if (githubClientUserRepositoriesGetResponseBody.isEmpty()) {
                                return Collections.emptyList();
                            }
                            return githubClientUserRepositoriesGetResponseBody
                                    .stream()
                                    .map(repo ->
                                            new GithubUserRepositoryModel(
                                                    String.valueOf(repo.getId()),
                                                    repo.getNodeId(),
                                                    repo.getName(),
                                                    repo.getFullName(),
                                                    String.valueOf(repo.getPrivate()),
                                                    repo.getHtmlUrl(),
                                                    repo.getDescription()
                                            )
                                    )
                                    .collect(Collectors.toList());
                        }
                );
    }
}
