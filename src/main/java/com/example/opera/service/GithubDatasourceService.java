package com.example.opera.service;

import com.example.opera.datasource.GithubUserEntity;
import com.example.opera.datasource.GithubUserRepository;
import com.example.opera.datasource.GithubUserRepositoryEntity;
import com.example.opera.datasource.GithubUserRepositoryRepository;
import com.example.opera.util.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.context.Context;
import reactor.util.context.ContextView;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class GithubDatasourceService {

    private Logger log = LoggerFactory.getLogger(GithubDatasourceService.class);

    private Scheduler dbThread = Schedulers.newBoundedElastic(4, 4, "db-threads");

    @Value("${opera.h2.actualTime}")
    private Long actualTime;

    private GithubUserRepository githubUserRepository;
    private GithubUserRepositoryRepository githubUserRepositoryRepository;

    public GithubDatasourceService(GithubUserRepository githubUserRepository, GithubUserRepositoryRepository githubUserRepositoryRepository) {
        this.githubUserRepository = githubUserRepository;
        this.githubUserRepositoryRepository = githubUserRepositoryRepository;
    }

    /**
     * @param login
     * @return tuple which contains UserEntity and Actual flag
     */
    public Mono<Optional<Tuple2<GithubUserEntity, Boolean>>> getUser(String login) {
        return githubUserRepository.findById(login)
                .map(user -> {
                    if (user.getCreateTime() < (Instant.now().plusMillis(actualTime)).getEpochSecond()) {
                        return Optional.of(new Tuple2<>(user, Boolean.TRUE));
                    }
                    return Optional.of(new Tuple2<>(user, Boolean.FALSE));
                })
                .switchIfEmpty(Mono.just(Optional.empty()));
    }

    /**
     * @param login
     * @return tuple which contains UserRepositoryEntity and Actual flag
     */
    public Mono<List<Tuple2<GithubUserRepositoryEntity, Boolean>>> getUserRepositories(String login) {
        return githubUserRepositoryRepository.findByLogin(login)
                .map(user -> {
                    if (user.getCreateTime() < (Instant.now().plusMillis(actualTime).getEpochSecond())) {
                        return Optional.of(new Tuple2<>(user, Boolean.TRUE));
                    }
                    return Optional.of(new Tuple2<>(user, Boolean.FALSE));
                })
                .switchIfEmpty(Mono.just(Optional.empty()))
                .filter(op -> op.isPresent())
                .map(Optional::get)
                .collectList();
    }

    public void saveUserEntity(ContextView ctx, GithubUserEntity githubUserEntity) {
        Mono.deferContextual(Mono::just)
                .publishOn(dbThread)
                //.flatMap(contextView -> githubUserRepository.delete(githubUserEntity))
                .flatMap(nothing -> githubUserRepository.save(githubUserEntity))
                .subscribe(contextView -> {}, ex -> log.error("Error while saving: {}", ex.getLocalizedMessage()), () -> {}, Context.of(ctx));
    }

    public void saveUserRepositoriesEntity(ContextView ctx, String login, List<GithubUserRepositoryEntity> githubUserRepositoryEntities) {
        Mono.deferContextual(Mono::just)
                .publishOn(dbThread)
                //.flatMapMany(contextView -> githubUserRepositoryRepository.deleteByLogin(login))
                .flatMapMany(nothing -> githubUserRepositoryRepository.saveAll(githubUserRepositoryEntities))
                .subscribe(contextView -> {}, ex -> log.error("Error while saving: {}", ex.getLocalizedMessage()), () -> {}, Context.of(ctx));
    }
}
