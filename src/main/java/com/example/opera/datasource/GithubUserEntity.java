package com.example.opera.datasource;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.Objects;


@Table
public class GithubUserEntity implements Persistable<String> {

    @Id
    private String login;
    private String htmlUrl;
    private String note;


    private Long createTime;

    @Transient
    private boolean newProduct;

    public GithubUserEntity() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public static GithubUserEntity of(
            String login,
            String htmlUrl,
            String note
    ) {
        GithubUserEntity githubUserEntity = new GithubUserEntity();
        githubUserEntity.login = login;
        githubUserEntity.htmlUrl = htmlUrl;
        githubUserEntity.note = note;
        githubUserEntity.createTime = Instant.now().getEpochSecond();
        githubUserEntity.newProduct = true;
        return githubUserEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GithubUserEntity that = (GithubUserEntity) o;
        return Objects.equals(login, that.login);
    }

    @Override
    public String getId() {
        return login;
    }

    @Override
    @Transient
    public boolean isNew() {
        return this.newProduct;
    }
}
