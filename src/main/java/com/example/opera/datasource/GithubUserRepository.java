package com.example.opera.datasource;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface GithubUserRepository extends ReactiveCrudRepository<GithubUserEntity, String> {
}
