package com.example.opera.datasource;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

// I believe that is quite a good name for class almost as a FactoryFactoryFactory in Spring
public interface GithubUserRepositoryRepository extends ReactiveCrudRepository<GithubUserRepositoryEntity, String> {

    Flux<GithubUserRepositoryEntity> findByLogin(String login);

    Flux<Void> deleteByLogin(String login);

}
