package com.example.opera.datasource;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;

@Table
public class GithubUserRepositoryEntity implements Persistable<String> {

    @Id
    private String id;
    private String login;
    private String nodeId;
    private String name;
    private String fullName;
    private String isPrivate;
    private String htmlUrl;
    private String description;

    private Long createTime;

    @Transient
    private boolean newProduct;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return this.newProduct;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static GithubUserRepositoryEntity of(
            String id,
            String login,
            String nodeId,
            String name,
            String isPrivate,
            String fullName,
            String htmlUrl,
            String description
    ) {
        GithubUserRepositoryEntity githubUserRepositoryEntity = new GithubUserRepositoryEntity();
        githubUserRepositoryEntity.id = id;
        githubUserRepositoryEntity.login = login;
        githubUserRepositoryEntity.nodeId = nodeId;
        githubUserRepositoryEntity.name = name;
        githubUserRepositoryEntity.isPrivate = isPrivate;
        githubUserRepositoryEntity.fullName = fullName;
        githubUserRepositoryEntity.htmlUrl = htmlUrl;
        githubUserRepositoryEntity.description = description;
        githubUserRepositoryEntity.createTime = Instant.now().getEpochSecond();
        githubUserRepositoryEntity.newProduct = true;
        return githubUserRepositoryEntity;
    }

}
