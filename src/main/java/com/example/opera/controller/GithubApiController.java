package com.example.opera.controller;

import com.example.opera.model.GithubUserRepositoriesResponseBody;
import com.example.opera.model.GithubUserRepositoryModel;
import com.example.opera.model.GithubUserResponseBody;
import com.example.opera.service.GithubService;
import com.example.operagithubclient.client.GithubClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class GithubApiController {

    private final GithubService githubService;

    public GithubApiController(GithubService githubService) {
        this.githubService = githubService;
    }


    /**
     * @param client
     * @param userName
     * @param fetchInfo
     * @return
     */
    @GetMapping("/api/v1/user/{userName}")
    public Mono<GithubUserResponseBody> getUser(
            @RegisteredOAuth2AuthorizedClient("github") OAuth2AuthorizedClient client,
            @PathVariable String userName,
            @RequestParam(required = false) Boolean fetchInfo) {
        return githubService.getUserInfo(userName, fetchInfo)
                .contextWrite(ctx -> {
                    return ctx.put(GithubClient.GITHUB_CONTEXT_CLIENT, client);
                });
    }

    /**
     * @param client
     * @param userName
     * @param fetchInfo
     * @return
     */
    @GetMapping("/api/v1/repositories/{userName}")
    public Mono<GithubUserRepositoriesResponseBody> getUserRepositories(
            @RegisteredOAuth2AuthorizedClient("github") OAuth2AuthorizedClient client,
            @PathVariable String userName,
            @RequestParam(required = false) Boolean fetchInfo) {
        return githubService.getUserReposInfo(userName, fetchInfo)
                .doOnNext(r -> System.out.println(r))
                .contextWrite(ctx -> {
                    return ctx.put(GithubClient.GITHUB_CONTEXT_CLIENT, client);
                });
    }
}
