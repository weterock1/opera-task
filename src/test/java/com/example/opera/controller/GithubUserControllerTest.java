package com.example.opera.controller;

import com.example.opera.service.GithubDatasourceService;
import com.example.opera.service.GithubService;
import com.example.operagithubclient.client.GithubClient;
import com.example.operagithubclient.client.model.GithubClientUserGetResponseBody;
import com.example.operagithubclient.client.model.GithubClientUserRepositoriesGetResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.matches;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockOidcLogin;

@SpringBootTest
@AutoConfigureWebTestClient
public class GithubUserControllerTest {

    private Supplier<String> userAnswer = () -> {
        return "{\n" +
                "  \"login\": \"weterock\",\n" +
                "  \"id\": 22843070,\n" +
                "  \"node_id\": \"MDQ6VXNlcjIyODQzMDcw\",\n" +
                "  \"avatar_url\": \"https://avatars.githubusercontent.com/u/22843070?v=4\",\n" +
                "  \"gravatar_id\": \"\",\n" +
                "  \"url\": \"https://api.github.com/users/weterock\",\n" +
                "  \"html_url\": \"https://github.com/weterock\",\n" +
                "  \"followers_url\": \"https://api.github.com/users/weterock/followers\",\n" +
                "  \"following_url\": \"https://api.github.com/users/weterock/following{/other_user}\",\n" +
                "  \"gists_url\": \"https://api.github.com/users/weterock/gists{/gist_id}\",\n" +
                "  \"starred_url\": \"https://api.github.com/users/weterock/starred{/owner}{/repo}\",\n" +
                "  \"subscriptions_url\": \"https://api.github.com/users/weterock/subscriptions\",\n" +
                "  \"organizations_url\": \"https://api.github.com/users/weterock/orgs\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/weterock/repos\",\n" +
                "  \"events_url\": \"https://api.github.com/users/weterock/events{/privacy}\",\n" +
                "  \"received_events_url\": \"https://api.github.com/users/weterock/received_events\",\n" +
                "  \"type\": \"User\",\n" +
                "  \"site_admin\": false,\n" +
                "  \"name\": null,\n" +
                "  \"company\": null,\n" +
                "  \"blog\": \"\",\n" +
                "  \"location\": null,\n" +
                "  \"email\": null,\n" +
                "  \"hireable\": null,\n" +
                "  \"bio\": null,\n" +
                "  \"twitter_username\": null,\n" +
                "  \"public_repos\": 1,\n" +
                "  \"public_gists\": 0,\n" +
                "  \"followers\": 0,\n" +
                "  \"following\": 0,\n" +
                "  \"created_at\": \"2016-10-14T17:22:44Z\",\n" +
                "  \"updated_at\": \"2022-04-07T14:48:13Z\"\n" +
                "}";
    };

    private Supplier<String> userReposAnswer = () -> {
        return "[\n" +
                "  {\n" +
                "    \"id\": 472391293,\n" +
                "    \"node_id\": \"R_kgDOHCgefQ\",\n" +
                "    \"name\": \"atom\",\n" +
                "    \"full_name\": \"weterock/atom\",\n" +
                "    \"private\": false,\n" +
                "    \"owner\": {\n" +
                "      \"login\": \"weterock\",\n" +
                "      \"id\": 22843070,\n" +
                "      \"node_id\": \"MDQ6VXNlcjIyODQzMDcw\",\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/22843070?v=4\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/weterock\",\n" +
                "      \"html_url\": \"https://github.com/weterock\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/weterock/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/weterock/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/weterock/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/weterock/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/weterock/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/weterock/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/weterock/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/weterock/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/weterock/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"html_url\": \"https://github.com/weterock/atom\",\n" +
                "    \"description\": null,\n" +
                "    \"fork\": true,\n" +
                "    \"url\": \"https://api.github.com/repos/weterock/atom\",\n" +
                "    \"forks_url\": \"https://api.github.com/repos/weterock/atom/forks\",\n" +
                "    \"keys_url\": \"https://api.github.com/repos/weterock/atom/keys{/key_id}\",\n" +
                "    \"collaborators_url\": \"https://api.github.com/repos/weterock/atom/collaborators{/collaborator}\",\n" +
                "    \"teams_url\": \"https://api.github.com/repos/weterock/atom/teams\",\n" +
                "    \"hooks_url\": \"https://api.github.com/repos/weterock/atom/hooks\",\n" +
                "    \"issue_events_url\": \"https://api.github.com/repos/weterock/atom/issues/events{/number}\",\n" +
                "    \"events_url\": \"https://api.github.com/repos/weterock/atom/events\",\n" +
                "    \"assignees_url\": \"https://api.github.com/repos/weterock/atom/assignees{/user}\",\n" +
                "    \"branches_url\": \"https://api.github.com/repos/weterock/atom/branches{/branch}\",\n" +
                "    \"tags_url\": \"https://api.github.com/repos/weterock/atom/tags\",\n" +
                "    \"blobs_url\": \"https://api.github.com/repos/weterock/atom/git/blobs{/sha}\",\n" +
                "    \"git_tags_url\": \"https://api.github.com/repos/weterock/atom/git/tags{/sha}\",\n" +
                "    \"git_refs_url\": \"https://api.github.com/repos/weterock/atom/git/refs{/sha}\",\n" +
                "    \"trees_url\": \"https://api.github.com/repos/weterock/atom/git/trees{/sha}\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/weterock/atom/statuses/{sha}\",\n" +
                "    \"languages_url\": \"https://api.github.com/repos/weterock/atom/languages\",\n" +
                "    \"stargazers_url\": \"https://api.github.com/repos/weterock/atom/stargazers\",\n" +
                "    \"contributors_url\": \"https://api.github.com/repos/weterock/atom/contributors\",\n" +
                "    \"subscribers_url\": \"https://api.github.com/repos/weterock/atom/subscribers\",\n" +
                "    \"subscription_url\": \"https://api.github.com/repos/weterock/atom/subscription\",\n" +
                "    \"commits_url\": \"https://api.github.com/repos/weterock/atom/commits{/sha}\",\n" +
                "    \"git_commits_url\": \"https://api.github.com/repos/weterock/atom/git/commits{/sha}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/weterock/atom/comments{/number}\",\n" +
                "    \"issue_comment_url\": \"https://api.github.com/repos/weterock/atom/issues/comments{/number}\",\n" +
                "    \"contents_url\": \"https://api.github.com/repos/weterock/atom/contents/{+path}\",\n" +
                "    \"compare_url\": \"https://api.github.com/repos/weterock/atom/compare/{base}...{head}\",\n" +
                "    \"merges_url\": \"https://api.github.com/repos/weterock/atom/merges\",\n" +
                "    \"archive_url\": \"https://api.github.com/repos/weterock/atom/{archive_format}{/ref}\",\n" +
                "    \"downloads_url\": \"https://api.github.com/repos/weterock/atom/downloads\",\n" +
                "    \"issues_url\": \"https://api.github.com/repos/weterock/atom/issues{/number}\",\n" +
                "    \"pulls_url\": \"https://api.github.com/repos/weterock/atom/pulls{/number}\",\n" +
                "    \"milestones_url\": \"https://api.github.com/repos/weterock/atom/milestones{/number}\",\n" +
                "    \"notifications_url\": \"https://api.github.com/repos/weterock/atom/notifications{?since,all,participating}\",\n" +
                "    \"labels_url\": \"https://api.github.com/repos/weterock/atom/labels{/name}\",\n" +
                "    \"releases_url\": \"https://api.github.com/repos/weterock/atom/releases{/id}\",\n" +
                "    \"deployments_url\": \"https://api.github.com/repos/weterock/atom/deployments\",\n" +
                "    \"created_at\": \"2022-03-21T15:11:10Z\",\n" +
                "    \"updated_at\": \"2016-10-12T14:46:25Z\",\n" +
                "    \"pushed_at\": \"2016-12-20T17:25:17Z\",\n" +
                "    \"git_url\": \"git://github.com/weterock/atom.git\",\n" +
                "    \"ssh_url\": \"git@github.com:weterock/atom.git\",\n" +
                "    \"clone_url\": \"https://github.com/weterock/atom.git\",\n" +
                "    \"svn_url\": \"https://github.com/weterock/atom\",\n" +
                "    \"homepage\": null,\n" +
                "    \"size\": 33113,\n" +
                "    \"stargazers_count\": 0,\n" +
                "    \"watchers_count\": 0,\n" +
                "    \"language\": null,\n" +
                "    \"has_issues\": false,\n" +
                "    \"has_projects\": true,\n" +
                "    \"has_downloads\": true,\n" +
                "    \"has_wiki\": true,\n" +
                "    \"has_pages\": false,\n" +
                "    \"forks_count\": 0,\n" +
                "    \"mirror_url\": null,\n" +
                "    \"archived\": false,\n" +
                "    \"disabled\": false,\n" +
                "    \"open_issues_count\": 0,\n" +
                "    \"license\": null,\n" +
                "    \"allow_forking\": true,\n" +
                "    \"is_template\": false,\n" +
                "    \"topics\": [\n" +
                "\n" +
                "    ],\n" +
                "    \"visibility\": \"public\",\n" +
                "    \"forks\": 0,\n" +
                "    \"open_issues\": 0,\n" +
                "    \"watchers\": 0,\n" +
                "    \"default_branch\": \"master\"\n" +
                "  }\n" +
                "]";
    };

    private Supplier<String> userControllerResponse = () -> {
        return "{\"login\":\"weterock\",\"htmlUrl\":\"https://github.com/weterock\",\"note\":null,\"isActual\":true}";
    };

    private Supplier<String> userRepositoriesControllerResponse = () -> {
        return "{\"repositories\":[{\"id\":\"472391293\",\"nodeId\":\"R_kgDOHCgefQ\",\"name\":\"atom\",\"fullName\":\"weterock/atom\",\"isPrivate\":\"false\",\"htmlUrl\":\"https://github.com/weterock/atom\",\"description\":null}],\"actual\":true}";
    };

    @SpyBean
    private WebTestClient webTestClient;
    @SpyBean
    private GithubService githubService;
    @SpyBean
    private GithubDatasourceService githubDatasourceService;
    @SpyBean
    private ObjectMapper objectMapper;

    @MockBean
    private GithubClient githubClient;


    @BeforeEach
    public void initMock() throws JsonProcessingException {
        GithubClientUserGetResponseBody userResponse =
                objectMapper.readValue(userAnswer.get(), GithubClientUserGetResponseBody.class);

        List<GithubClientUserRepositoriesGetResponseBody> userRepositoriesResponse =
                objectMapper.readValue(userReposAnswer.get(), new TypeReference<List<GithubClientUserRepositoriesGetResponseBody>>() {
                });

        webTestClient = webTestClient.mutate()
                .responseTimeout(
                        Duration.ofMillis(30000L)
                )
                .build();
        Mockito.when(githubClient.<GithubClientUserGetResponseBody>sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*"), any()))
                .thenReturn(
                        Mono.just(userResponse)
                );
        Mockito.when(githubClient.<List<GithubClientUserRepositoriesGetResponseBody>>sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*/repos"), any()))
                .thenReturn(
                        Mono.just(userRepositoriesResponse)
                );
    }

    @Test
    public void getNoAuthReturnsRedirectLogin() {
        webTestClient.get().uri("/api/v1/repositories/weterock")
                .exchange()
                .expectStatus().is3xxRedirection();
    }

    @Test
    public void testUserInfoCall() {
        webTestClient
                .mutateWith(mockOidcLogin().idToken(token -> token.claim("name", "Mock User")))
                .method(
                        HttpMethod.GET
                )
                .uri("/api/v1/user/weterock")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(userControllerResponse.get());

        Mockito.verify(githubClient, Mockito.times(1)).sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*"), any());
        Mockito.verify(githubService, Mockito.times(1)).getUserInfo(any(), ArgumentMatchers.isNull());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).saveUserEntity(any(), any());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).getUser(any());

        webTestClient
                .mutateWith(mockOidcLogin().idToken(token -> token.claim("name", "Mock User")))
                .method(
                        HttpMethod.GET
                )
                .uri("/api/v1/user/weterock")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(userControllerResponse.get());

        Mockito.verify(githubClient, Mockito.times(1)).sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*"), any());
        Mockito.verify(githubService, Mockito.times(2)).getUserInfo(any(), ArgumentMatchers.isNull());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).saveUserEntity(any(), any());
        Mockito.verify(githubDatasourceService, Mockito.times(2)).getUser(any());
    }

    @Test
    public void testUserRepositoriesInfoCall() {
        webTestClient
                .mutateWith(mockOidcLogin().idToken(token -> token.claim("name", "Mock User")))
                .method(
                        HttpMethod.GET
                )
                .uri("/api/v1/repositories/weterock")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(userRepositoriesControllerResponse.get());

        Mockito.verify(githubClient, Mockito.times(1)).sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*"), any());
        Mockito.verify(githubService, Mockito.times(1)).getUserReposInfo(any(), ArgumentMatchers.isNull());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).saveUserRepositoriesEntity(any(), any(), any());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).getUserRepositories(any());

        webTestClient
                .mutateWith(mockOidcLogin().idToken(token -> token.claim("name", "Mock User")))
                .method(
                        HttpMethod.GET
                )
                .uri("/api/v1/repositories/weterock")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(userRepositoriesControllerResponse.get());

        Mockito.verify(githubClient, Mockito.times(1)).sendGetGithubRequest(matches("/users/[a-zA-Z0-9]*"), any());
        Mockito.verify(githubService, Mockito.times(2)).getUserReposInfo(any(), ArgumentMatchers.isNull());
        Mockito.verify(githubDatasourceService, Mockito.times(1)).saveUserRepositoriesEntity(any(), any(), any());
        Mockito.verify(githubDatasourceService, Mockito.times(2)).getUserRepositories(any());
    }
}
